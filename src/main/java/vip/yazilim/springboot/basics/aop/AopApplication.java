package vip.yazilim.springboot.basics.aop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class AopApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(AopApplication.class, args);

		// aspect
		Business busenss = ctx.getBean(Business.class);

		// testing aspect
		busenss.getARecord();
		try {

			busenss.wrong();
		} catch (Exception e) {
			System.out.println("An exception occur on Business Object\n");
		}

	}

}
